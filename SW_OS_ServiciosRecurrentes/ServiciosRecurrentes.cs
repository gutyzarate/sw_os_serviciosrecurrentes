﻿using SW_OS_ServiciosRecurrentes.Models.Context;
using SW_OS_ServiciosRecurrentes.Models.ModelResult;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Configuration;

namespace SW_OS_ServiciosRecurrentes
{
    partial class ServiciosRecurrentes : ServiceBase
    {
        bool enProceso = false;
        private Timer Schedular;
        private OS_DBContext db = new OS_DBContext();
        private ELI_DBContext eliDB = new ELI_DBContext();
        public ServiciosRecurrentes()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            
            EventLog.WriteEntry("Se inicia proceso de verificación de Servicios Recurrentes", EventLogEntryType.Information);
            //stLapso.Start();
            this.VerificarServiciosRecurrentes();
        }

        protected override void OnStop()
        {
            
            EventLog.WriteEntry("Finalizó proceso de verificación de Servicios Recurrentes", EventLogEntryType.Information);
            this.Schedular.Dispose();
        }

        //private void verificarServiciosRecurrentes(object sender, System.Timers.ElapsedEventArgs e)
        private void VerificarServiciosRecurrentes()
        {
            try
            {
                Schedular = new Timer(new TimerCallback(SchedularCallback));
                string mode = ConfigurationManager.AppSettings["Mode"].ToUpper();
                //Set the Default Time.
                DateTime scheduledTime = DateTime.MinValue;
                if (mode == "DAILY")
                {
                    //Get the Scheduled Time from AppSettings.
                    scheduledTime = DateTime.Parse(ConfigurationManager.AppSettings["ScheduledTime"]);
                    EventLog.WriteEntry("Fecha Programada de revisión: "+ scheduledTime.ToString("yyyy-MM-dd HH:mm")+ "\nHora Servidor: "+ DateTime.Now.ToString("yyyy-MM-dd HH:mm"), EventLogEntryType.Information);
                    if (DateTime.Now > scheduledTime)
                    {
                        //If Scheduled Time is passed set Schedule for the next day.
                        scheduledTime = scheduledTime.AddDays(1);
                        EventLog.WriteEntry("Inicia Revisión: "+ DateTime.Now.ToString("yyyy-MM-dd HH:mm"), EventLogEntryType.Information);
                        EventLog.WriteEntry("Próxima revisión: " + scheduledTime.ToString("yyyy-MM-dd HH:mm"), EventLogEntryType.Information);
                        enProceso = true;
                    }
                    else
                    {
                        enProceso = false;
                    }
                }

                if (mode.ToUpper() == "INTERVAL")
                {
                    //Get the Interval in Minutes from AppSettings.
                    int intervalMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["IntervalMinutes"]);

                    //Set the Scheduled Time by adding the Interval to Current Time.
                    scheduledTime = DateTime.Now.AddMinutes(intervalMinutes);
                    if (DateTime.Now > scheduledTime)
                    {
                        //If Scheduled Time is passed set Schedule for the next Interval.
                        scheduledTime = scheduledTime.AddMinutes(intervalMinutes);
                        enProceso = false;
                    }
                    else
                    {
                        enProceso = true;
                    }
                }

                TimeSpan timeSpan = scheduledTime.Subtract(DateTime.Now);
                //Get the difference in Minutes between the Scheduled and Current Time.
                int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);

                //Change the Timer's Due Time.
                Schedular.Change(dueTime, Timeout.Infinite);
            }
            catch (Exception ex)
            {
                var message = "Mensaje error: \n" + ex.Message + " En línea: \n" + (new StackTrace(ex, true)).GetFrame(0).GetFileLineNumber() + " \n\nTrazo error: \n" + ex.StackTrace + " \n\nFuente error: \n" + ex.Source + " \n\nMetodo error: \n" + ex.TargetSite + " \n\nInner error: \n" + ex.InnerException;
                EventLog.WriteEntry(message, EventLogEntryType.Error);
                using (ServiceController serviceController = new System.ServiceProcess.ServiceController("VerificarServiciosRecurrentes"))
                {
                    serviceController.Stop();
                }
            }

            if (!enProceso) return;

            try
            {
                //var ServiciosRecurrentes = new verificarServiciosRecurrentes();
                //ServiciosRecurrentes.verificar();
                db.Database.CommandTimeout = 10000;
                var query = "SELECT * FROM OS_tblServiciosRecurrentes WHERE eliminado_el IS NULL";
                List<ServicioRecurrente> serviciosRecurrentes = db.Database.SqlQuery<ServicioRecurrente>(query).ToList();
                if(serviciosRecurrentes.Count() == 0)
                {
                    EventLog.WriteEntry("No hay Servicios Recurrentes", EventLogEntryType.Information);
                    return;
                }
                foreach (ServicioRecurrente servicioRecurrente in serviciosRecurrentes)
                {
                    if(servicioRecurrente.fecha_inicial == null)
                    {
                        EventLog.WriteEntry("El Servicio Recurrente con Id: " + servicioRecurrente.id +" necesita una fecha inicial, favor de verificarlo.", EventLogEntryType.Warning);
                        continue;
                    }
                    if (servicioRecurrente.fecha_final == null)
                    {
                        EventLog.WriteEntry("El Servicio Recurrente con Id: " + servicioRecurrente.id + " no tiene configurado una fecha final de recurrencia, favor de verificarlo.", EventLogEntryType.Warning);
                    }
                    if (String.IsNullOrEmpty(servicioRecurrente.aduana_id))
                    {
                        EventLog.WriteEntry("El Servicio Recurrente con Id: " + servicioRecurrente.id + " requiere de el id de aduana para continuar, favor de verificarlo.", EventLogEntryType.Warning);
                        continue;
                    }
                    if (String.IsNullOrEmpty(servicioRecurrente.seccion_aduana))
                    {
                        EventLog.WriteEntry("El Servicio Recurrente con Id: " + servicioRecurrente.id + " requiere de la seccion de aduana para continuar, favor de verificarlo.", EventLogEntryType.Warning);
                        continue;
                    }
                    bool primeraOS = false;
                    int periodoDias = servicioRecurrente.frecuencia;
                    DateTime fecha_inicial = Convert.ToDateTime(servicioRecurrente.fecha_inicial.ToString()).Date;
                    DateTime fecha_final = Convert.ToDateTime(servicioRecurrente.fecha_final.ToString()).Date;
                    DateTime fecha_proximo_evento;
                    DateTime hoy = DateTime.Now.Date;

                    var diasParaIniciarVigencia = (fecha_inicial - hoy).TotalDays;
                    var diasParaFinalizarVigencia = (fecha_final - hoy).TotalDays;

                    if (diasParaFinalizarVigencia < 0)
                    {
                        db.Database.ExecuteSqlCommand("UPDATE OS_tblServiciosRecurrentes SET actualizado_el = CURRENT_TIMESTAMP, fecha_proximo_evento = NULL WHERE id = " + servicioRecurrente.id);
                        EventLog.WriteEntry("El Servicio Recurrente con Id: " + servicioRecurrente.id + " ha caducado.", EventLogEntryType.Information);
                        continue;
                    }

                    if (diasParaIniciarVigencia <= 0)
                    {
                        if (servicioRecurrente.fecha_proximo_evento == null)
                        {
                            EventLog.WriteEntry("El Servicio Recurrente con Id: " + servicioRecurrente.id + " entra en vigor", EventLogEntryType.Information);
                        }
                        else
                        {
                            EventLog.WriteEntry("El Servicio Recurrente con Id: " + servicioRecurrente.id + " continua vigente. Restan " + diasParaFinalizarVigencia + " días para finalizar.", EventLogEntryType.Information);
                        }
                    }
                    else
                    {
                        EventLog.WriteEntry("El Servicio Recurrente con Id: " + servicioRecurrente.id + " aun no entra en vigor. Restan " + diasParaIniciarVigencia + " días para iniciar", EventLogEntryType.Information);
                        continue;
                    }

                    if (servicioRecurrente.fecha_proximo_evento == null)
                    {
                        primeraOS = true;
                        if ((fecha_inicial - hoy).TotalDays <= 0) {
                            fecha_proximo_evento = hoy.AddDays(periodoDias);
                        }
                        else
                        {
                            fecha_proximo_evento = fecha_inicial.AddDays(periodoDias);
                        }
                        
                        db.Database.ExecuteSqlCommand("UPDATE OS_tblServiciosRecurrentes SET actualizado_el = CURRENT_TIMESTAMP, fecha_proximo_evento ='" + fecha_proximo_evento.ToString("yyyy-MM-dd HH:mm") + "' WHERE id = " + servicioRecurrente.id);
                    }
                    else
                    {
                        fecha_proximo_evento = DateTime.Parse(servicioRecurrente.fecha_proximo_evento.ToString());
                    }

                    var diasParaProximoEvento = (fecha_proximo_evento - hoy).TotalDays;

                    if (primeraOS || diasParaProximoEvento <= 0)
                    {
                        // OBtenemos datos del concepto 
                        query = "SELECT * FROM OS_tblConceptos WHERE id = " + servicioRecurrente.concepto_id;
                        Concepto concepto = db.Database.SqlQuery<Concepto>(query).First();

                        // Creamos el concepto
                        OS_OrdenServicioConcepto ordenServicioConcepto = new OS_OrdenServicioConcepto();
                        if(servicioRecurrente.usuario_alta != null)
                        {
                            ordenServicioConcepto.usuario_alta = servicioRecurrente.usuario_alta;
                        }
                        ordenServicioConcepto.concepto_id = servicioRecurrente.concepto_id;
                        ordenServicioConcepto.realizado = true;
                        ordenServicioConcepto.acuerdo = false;
                        ordenServicioConcepto.facturar_al_cliente = true;
                        // Vaildamos la cantidad a usar
                        var cantidadConceptoOS = 1;
                        if (Convert.ToBoolean(servicioRecurrente.acuerdo_de_cliente))
                        {
                            // OBtenemos datos del acuerdo 
                            query = "SELECT " +
                                "OS_tblAcuerdos.*, " +
                                "RTRIM(LTRIM(OS_tblConceptos.concepto_gp)) as concepto_gp " +
                                "FROM OS_tblAcuerdos " +
                                "LEFT JOIN OS_tblConceptos " +
                                "ON OS_tblConceptos.id = OS_tblAcuerdos.concepto_id " +
                                "WHERE " +
                                "OS_tblAcuerdos.concepto_id = " + servicioRecurrente.concepto_id+" AND " +
                                "OS_tblAcuerdos.cliente_id = " + servicioRecurrente.cliente_id+" AND " +
                                "OS_tblAcuerdos.sitio_id = " + servicioRecurrente.sitio_id;
                            Acuerdo acuerdo = db.Database.SqlQuery<Acuerdo>(query).FirstOrDefault();
                            if(acuerdo != null)
                            {
                                ordenServicioConcepto.acuerdo = true;
                                cantidadConceptoOS = Convert.ToInt32(acuerdo.numero_de_personas);
                            }
                        }
                        ordenServicioConcepto.cantidad = cantidadConceptoOS;
                        ordenServicioConcepto.usuario = 1;

                        // Obtenemos datos del cliente 
                        query = "select CUSTNMBR, CUSTNAME from RM00101 WHERE CUSTNMBR = " + servicioRecurrente.cliente_id;
                        ELI_Clientes cliente = eliDB.Database.SqlQuery<ELI_Clientes>(query).First();
                        string CUSTNMBR = cliente.CUSTNMBR;
                        string CUSTNAME = cliente.CUSTNAME;
                        // Termina Obtenemos datos del cliente 

                        // Obtenemos datos del tipo de operacion IMPOEXP 
                        query = "select id, etiqueta from OS_tblTiposOperacion WHERE id = " + servicioRecurrente.tipo_operacion_id;
                        OS_TiposOperacion tipo_operacion = db.Database.SqlQuery<OS_TiposOperacion>(query).First();
                        string IMPOEXP = tipo_operacion.etiqueta.Remove(tipo_operacion.etiqueta.Length - 1);
                        // Termina Obtenemos datos del tipo de operacion IMPOEXP 

                        // Obtenemos datos del empresa 
                        query = "SELECT OS_tblEmpresas.* FROM OS_tblEmpresas JOIN OS_tblSitios ON OS_tblSitios.empresa_id = OS_tblEmpresas.id WHERE OS_tblSitios.id = " + servicioRecurrente.sitio_id;
                        OS_Empresas empresa = db.Database.SqlQuery<OS_Empresas>(query).First();
                        // Termina Obtenemos datos del empresa 

                        // Calculamos el LOCNCODE
                        String LOCNCODE = "";

                        // Obtenemos datos de la aduana 
                        query = "SELECT id_aduana, seccion_aduana, razon_social, razon_social_abreviada, descripcion_oficial from [ADUASIS].[dbo].[faduana] WHERE id_aduana = " + servicioRecurrente.aduana_id + " AND seccion_aduana = " + servicioRecurrente.seccion_aduana;
                        ADUASIS_Aduanas aduana = db.Database.SqlQuery<ADUASIS_Aduanas>(query).First();
                        String ADUASECC = aduana.seccion_aduana;
                        String DENOMC = aduana.razon_social;
                        // Termina Obtenemos datos de la aduana

                        switch (empresa.etiqueta)
                        {
                            case "ELI":
                                if (aduana.id_aduana + aduana.seccion_aduana == "800")
                                {
                                    LOCNCODE = "020";
                                }
                                else if (aduana.id_aduana == "17")
                                {
                                    LOCNCODE = "030";
                                }
                                else if (aduana.id_aduana == "24")
                                {
                                    LOCNCODE = "020";
                                }
                                else if (aduana.id_aduana + aduana.seccion_aduana == "270")
                                {
                                    LOCNCODE = "060";
                                }
                                else if (aduana.id_aduana + aduana.seccion_aduana == "271")
                                {
                                    LOCNCODE = "088";
                                }
                                else if (aduana.id_aduana == "30")
                                {
                                    LOCNCODE = "040";
                                }
                                else if (aduana.id_aduana == "52")
                                {
                                    LOCNCODE = "010";
                                }
                                else if (aduana.id_aduana == "48")
                                {
                                    LOCNCODE = "050";
                                }
                                else if (aduana.id_aduana == "43")
                                {
                                    LOCNCODE = "190";
                                }
                                else
                                {
                                    LOCNCODE = "050";
                                }
                                break;
                            case "EILO":
                                LOCNCODE = "0150";
                                break;
                            case "ALT":
                                LOCNCODE = "0140";
                                break;
                        }
                        // Termina Calculamos el LOCNCODE

                        // OBTENER REFERENCIA OP Y GP SI NO EXISTE
                        SqlConnection conexion = new SqlConnection(db.Database.Connection.ConnectionString);

                        // Llamamos el SP para validar el consecutivo de la referencia
                        var command = new SqlCommand("ELI.dbo.CSP_VER_CONSECUTIVO_INTERCOMPAÑIAS", conexion);
                        command.CommandType = CommandType.StoredProcedure;
                        var parametros = new List<SqlParameter>(){
                        new SqlParameter { ParameterName = "@CONSECUTIVO", SqlDbType = SqlDbType.Char, Direction = ParameterDirection.Output, Size = 33},
                    };
                        command.Parameters.AddRange(parametros.ToArray());
                        conexion.Open();
                        command.ExecuteNonQuery();
                        String Respuesta = Convert.ToString(command.Parameters["@CONSECUTIVO"].Value);
                        conexion.Close();
                        String GPREFEREN = Respuesta.Replace("EL-", "OS-"); // Esta es la nueva referencia a Crear

                        // Ahora insertamos la referencia en la tabla AA110100 para asignarcela al cliente
                        var RFTYPE = "PEDIMENTO";
                        DateTime horaActual = DateTime.Now;
                        var fecha = horaActual.Date;
                        var parametrosReferencia = new List<SqlParameter>(){
                        new SqlParameter { ParameterName = "@RFTYPE", SqlDbType = SqlDbType.Char, Value = RFTYPE },
                        new SqlParameter { ParameterName = "@LOCNCODE", SqlDbType = SqlDbType.Char, Value = LOCNCODE },
                        new SqlParameter { ParameterName = "@ADUASECC", SqlDbType = SqlDbType.Char, Value = (aduana.id_aduana + aduana.seccion_aduana) },
                        new SqlParameter { ParameterName = "@OPREFEREN", SqlDbType = SqlDbType.Char, Value = GPREFEREN },
                        new SqlParameter { ParameterName = "@GPREFEREN", SqlDbType = SqlDbType.Char, Value = GPREFEREN },
                        new SqlParameter { ParameterName = "@EXREFEREN", SqlDbType = SqlDbType.Char, Value = "" },
                        new SqlParameter { ParameterName = "@CUSTNMBR", SqlDbType = SqlDbType.Char, Value = CUSTNMBR },
                        new SqlParameter { ParameterName = "@CUSTNAME", SqlDbType = SqlDbType.Char, Value = CUSTNAME },
                        new SqlParameter { ParameterName = "@CUSTNMBR2", SqlDbType = SqlDbType.Char, Value = "" },
                        new SqlParameter { ParameterName = "@Customer_Name2", SqlDbType = SqlDbType.Char, Value = "" },
                        new SqlParameter { ParameterName = "@CREATDDT", SqlDbType = SqlDbType.DateTime, Value = fecha },
                        new SqlParameter { ParameterName = "@USER2ENT", SqlDbType = SqlDbType.Char, Value = "Inter" },
                        new SqlParameter { ParameterName = "@MODIFDT", SqlDbType = SqlDbType.DateTime, Value = "1900-01-01" },
                        new SqlParameter { ParameterName = "@USERTOMDF", SqlDbType = SqlDbType.Char, Value = "" },
                        new SqlParameter { ParameterName = "@RFSTATUS", SqlDbType = SqlDbType.SmallInt, Value = 0 },
                        new SqlParameter { ParameterName = "@LSTEDTDT", SqlDbType = SqlDbType.DateTime, Value = "1900-01-01" },
                        new SqlParameter { ParameterName = "@RFDOCUMENT", SqlDbType = SqlDbType.Char, Value = "" },
                        new SqlParameter { ParameterName = "@REFCRTMTH", SqlDbType = SqlDbType.SmallInt, Value = 0 },
                        new SqlParameter { ParameterName = "@OBSRVS", SqlDbType = SqlDbType.Char, Value = "" },
                        new SqlParameter { ParameterName = "@REGCEID", SqlDbType = SqlDbType.Char, Value = "" },
                        new SqlParameter { ParameterName = "@IMPOEXP", SqlDbType = SqlDbType.Char, Value = IMPOEXP },
                        new SqlParameter { ParameterName = "@REFORIG", SqlDbType = SqlDbType.SmallInt, Value = 0 },
                        new SqlParameter { ParameterName = "@REFAUX4", SqlDbType = SqlDbType.Decimal, Value = 0 },
                        new SqlParameter { ParameterName = "@REFAUX5", SqlDbType = SqlDbType.Decimal, Value = 0 },
                        new SqlParameter { ParameterName = "@REFAUX6", SqlDbType = SqlDbType.Decimal, Value = 0 },
                        new SqlParameter { ParameterName = "@OPRFNNC", SqlDbType = SqlDbType.TinyInt, Value = 0 },
                        new SqlParameter { ParameterName = "@DENOMC", SqlDbType = SqlDbType.NVarChar, Value = DENOMC },
                        new SqlParameter { ParameterName = "@PAGO_OP", SqlDbType = SqlDbType.VarChar, Value = 1 },
                        new SqlParameter { ParameterName = "@MONTO", SqlDbType = SqlDbType.Decimal, Value = 0 },
                        new SqlParameter { ParameterName = "@STATUS_OP", SqlDbType = SqlDbType.TinyInt, Value = 0 },
                        new SqlParameter { ParameterName = "@VENDORID", SqlDbType = SqlDbType.NVarChar, Value = "" },
                        new SqlParameter { ParameterName = "@RECIBO", SqlDbType = SqlDbType.NVarChar, Value = "" },
                        new SqlParameter { ParameterName = "@ARCHIVO", SqlDbType = SqlDbType.NVarChar, Value = "" },
                        new SqlParameter { ParameterName = "@MONEDA", SqlDbType = SqlDbType.NVarChar, Value = "" },
                        new SqlParameter { ParameterName = "@ID_RECEPTOR", SqlDbType = SqlDbType.NVarChar, Value = "" },
                        new SqlParameter { ParameterName = "@PATENTE", SqlDbType = SqlDbType.NVarChar, Value = "" },
                        new SqlParameter { ParameterName = "@PEDIMENTO", SqlDbType = SqlDbType.NVarChar, Value = "" },
                        new SqlParameter { ParameterName = "@PAGO", SqlDbType = SqlDbType.DateTime, Value = (object)DBNull.Value },
                        new SqlParameter { ParameterName = "@ETA", SqlDbType = SqlDbType.DateTime, Value = (object)DBNull.Value },
                        new SqlParameter { ParameterName = "@Transporte", SqlDbType = SqlDbType.Int, Value = (object)DBNull.Value },
                        new SqlParameter { ParameterName = "@Revalidacion", SqlDbType = SqlDbType.DateTime, Value = (object)DBNull.Value },
                        new SqlParameter { ParameterName = "@NIU", SqlDbType = SqlDbType.DateTime, Value = (object)DBNull.Value },
                        new SqlParameter { ParameterName = "@CAJAS", SqlDbType = SqlDbType.Int, Value = (object)DBNull.Value },
                        new SqlParameter { ParameterName = "@GUIAS", SqlDbType = SqlDbType.Int, Value = (object)DBNull.Value },
                        new SqlParameter { ParameterName = "@TRAFICOS", SqlDbType = SqlDbType.Int, Value = (object)DBNull.Value },
                        new SqlParameter { ParameterName = "@REMESAS", SqlDbType = SqlDbType.Int, Value = (object)DBNull.Value },
                        new SqlParameter { ParameterName = "@RESULTADO", SqlDbType = SqlDbType.NVarChar, Size = -1, Direction = ParameterDirection.Output},
                    };
                        // Creamos la referencia para Empresa Original
                        var commandEmpresaOriginal = new SqlCommand(empresa.etiqueta+".dbo.CSP_VER_AA110100_INS", conexion);
                        commandEmpresaOriginal.CommandType = CommandType.StoredProcedure;
                        commandEmpresaOriginal.Parameters.AddRange(parametrosReferencia.ToArray());
                        conexion.Open();
                        commandEmpresaOriginal.ExecuteNonQuery();
                        //Respuesta = Convert.ToString(command.Parameters["@RESULTADO"].Value);
                        conexion.Close();
                        commandEmpresaOriginal.Parameters.Clear();

                        // Creamos la referencia para Empresa ELI
                        if(empresa.etiqueta != "ELI")
                        {
                            var commandEli = new SqlCommand("ELI.dbo.CSP_VER_AA110100_INS", conexion);
                            commandEli.CommandType = CommandType.StoredProcedure;
                            commandEli.Parameters.AddRange(parametrosReferencia.ToArray());
                            conexion.Open();
                            commandEli.ExecuteNonQuery();
                            //Respuesta = Convert.ToString(command.Parameters["@RESULTADO"].Value);
                            conexion.Close();
                            commandEli.Parameters.Clear();
                        }

                        //return Json(Respuesta, JsonRequestBehavior.AllowGet);
                        // TERMINA OBTENER REFERENCIA OP Y GP SI NO EXISTE

                        // Creamos la OS
                        OS_OrdenServicio ordenServicio = new OS_OrdenServicio();
                        if(servicioRecurrente.usuario_alta != null)
                        {
                            ordenServicio.usuario_alta = servicioRecurrente.usuario_alta;
                        }
                        ordenServicio.referencia_op = GPREFEREN.Trim();
                        ordenServicio.referencia_gp = GPREFEREN.Trim();
                        ordenServicio.cliente_id = servicioRecurrente.cliente_id;
                        ordenServicio.sitio_id = servicioRecurrente.sitio_id;
                        ordenServicio.orden_servicio_tipo_referencia_id = 1;
                        ordenServicio.orden_servicio_tipo_id = 2;
                        ordenServicio.conceptos = new List<OS_OrdenServicioConcepto>();
                        ordenServicio.conceptos.Add(ordenServicioConcepto);

                        // Obtenemos los datos de Referencia OP
                        var valoresOpAA110100 = db.Database.SqlQuery<ELI_Facturacion>("SELECT TOP 1 IMPOEXP, ADUASECC, OPREFEREN, GPREFEREN " +
                    "from [ELI].[dbo].[AA110100] " +
                    "where RTRIM(LTRIM(UPPER(OPREFEREN))) = UPPER('" + GPREFEREN.Trim() + "')").FirstOrDefault();

                        var valoresOpAA310110 = db.Database.SqlQuery<ELI_Facturacion>("SELECT TOP 1 CLPEID, PEDNMBR " +
                        "from [ELI].[dbo].[AA310110] " +
                        "where RTRIM(LTRIM(UPPER(OPREFEREN))) = UPPER('" + GPREFEREN.Trim() + "')").FirstOrDefault();

                        ELI_Facturacion valoresOP = new ELI_Facturacion();
                        if (valoresOpAA110100 != null)
                        {
                            valoresOP.IMPOEXP = valoresOpAA110100.IMPOEXP;
                            valoresOP.ADUASECC = valoresOpAA110100.ADUASECC;
                            valoresOP.OPREFEREN = valoresOpAA110100.OPREFEREN;
                        }
                        if (valoresOpAA310110 != null)
                        {
                            valoresOP.CLPEID = valoresOpAA310110.CLPEID;
                            valoresOP.PEDNMBR = valoresOpAA310110.PEDNMBR;
                        }

                        ordenServicio.referencia_tipo = valoresOP.IMPOEXP;
                        ordenServicio.referencia_aduana = valoresOP.ADUASECC;
                        ordenServicio.referencia_clave = valoresOP.CLPEID;
                        ordenServicio.referencia_pedimento = valoresOP.PEDNMBR;
                        // Termina Obtenemos los datos de Referencia OP

                        db.OrdenServicio.Add(ordenServicio);
                        db.SaveChanges();


                        if (primeraOS)
                        {
                            if ((fecha_inicial - hoy).TotalDays <= 0)
                            {
                                fecha_proximo_evento = hoy.AddDays(periodoDias);
                            }
                            else
                            {
                                fecha_proximo_evento = fecha_inicial.AddDays(periodoDias);
                            }
                            EventLog.WriteEntry("Se creó la primera OS(id:"+ ordenServicio.id+ ") para el Servicios Recurrentes No. "+servicioRecurrente.id, EventLogEntryType.Information);
                            EventLog.WriteEntry("Próximo evento para el Servicios Recurrentes No. " + servicioRecurrente.id + ": " + fecha_proximo_evento.ToString("yyyy-MM-dd HH:mm"), EventLogEntryType.Information);
                        }
                        else
                        {
                            // Si se creó la orden, se inicia la cuenta de fecha de inicio y final
                            fecha_proximo_evento = fecha_proximo_evento.AddDays(periodoDias);
                            EventLog.WriteEntry("Se creó la OS(id:" + ordenServicio.id + ") para el Servicios Recurrentes No. " + servicioRecurrente.id, EventLogEntryType.Information);
                            EventLog.WriteEntry("Próximo evento para el Servicios Recurrentes No. " + servicioRecurrente.id +": "+ fecha_proximo_evento.ToString("yyyy-MM-dd HH:mm"), EventLogEntryType.Information);
                        }

                        db.Database.ExecuteSqlCommand("UPDATE OS_tblServiciosRecurrentes SET actualizado_el = CURRENT_TIMESTAMP, fecha_proximo_evento ='" + fecha_proximo_evento.ToString("yyyy-MM-dd HH:mm") + "' WHERE id = " + servicioRecurrente.id);
                    }

                }
                
            }
            catch(Exception ex)
            {
                var message = "Mensaje error: \n" + ex.Message + " En línea: \n" + (new StackTrace(ex, true)).GetFrame(0).GetFileLineNumber() + " \n\nTrazo error: \n" + ex.StackTrace + " \n\nFuente error: \n" + ex.Source + " \n\nMetodo error: \n" + ex.TargetSite + " \n\nInner error: \n" + ex.InnerException;
                EventLog.WriteEntry(message, EventLogEntryType.Error);
            }

            enProceso = false;
        }

        private void SchedularCallback(object e)
        {
            this.VerificarServiciosRecurrentes();
        }
    }
}
