﻿using SW_OS_ServiciosRecurrentes.Models.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW_OS_ServiciosRecurrentes.Models.ModelResult
{
    [Table("OS_tblOrdenesServicio")]
    public class OS_OrdenServicio
    {

        [Key]
        public long id { get; set; }
        public string referencia_op { get; set; }
        public string referencia_gp { get; set; }
        public string cliente_id { get; set; }
        public DateTime? fecha { get; set; }
        public string referencia_tipo { get; set; }
        public string referencia_aduana { get; set; }
        public string referencia_clave { get; set; }
        public string referencia_pedimento { get; set; }
        public string num_de_caja_o_guia { get; set; }
        public string embarque { get; set; }
        public string factura_comercial { get; set; }
        public string comentarios { get; set; }
        public long sitio_id { get; set; }
        public long? servicio_id { get; set; }
        public long orden_servicio_tipo_referencia_id { get; set; }
        public long orden_servicio_estatus_id { get; set; }
        public long orden_servicio_tipo_id { get; set; }
        public DateTime? creado_el { get; set; }
        public string mensaje_aclaracion_1 { get; set; }
        public string respuesta_aclaracion_1 { get; set; }
        public string mensaje_aclaracion_2 { get; set; }
        public string respuesta_aclaracion_2 { get; set; }
        public long? aviso_servicio_id { get; set; }
        public long? usuario_alta { get; set; }
        public long? usuario_modificacion { get; set; }
        public DateTime? actualizado_el { get; set; }

        public virtual ICollection<OS_OrdenServicioConcepto> conceptos { get; set; }

        public virtual string folio
        {
            get
            {
                return this.generarFolio(id);
            }
        }

        public string generarFolio(Int64? id)
        {
            return "O" + id.ToString().PadLeft(8, '0');
        }
    }
}
