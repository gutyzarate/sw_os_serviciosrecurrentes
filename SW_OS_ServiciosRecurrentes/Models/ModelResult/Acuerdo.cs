﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW_OS_ServiciosRecurrentes.Models.ModelResult
{
    class Acuerdo
    {
        public long id { get; set; }
        public string nombre_concepto { get; set; }
        public string nombre_concepto_personalizado { get; set; }
        public string destinatario_factura { get; set; }
        public byte estatus { get; set; }
        public string cliente_id { get; set; }
        public double? dias_libres { get; set; }
        public double? espacio_maximo_pies_cuadrados { get; set; }
        public int? minimo_de_piezas { get; set; }
        public int? numero_de_personas { get; set; }
        public string frecuencia_de_facturacion { get; set; }
        public string vigencia { get; set; }
        public int? minimo_de_horas { get; set; }
        public string horario_de_servicio_normal { get; set; }
        public byte facturar_tipo_receptor { get; set; }
        public long concepto_id { get; set; }
        [NotMapped]
        public string concepto_gp { get; set; }
        public long empresa_id { get; set; }
        public decimal concepto_importe { get; set; }
        public long divisa_id { get; set; }
        public long? sitio_id { get; set; }
        public bool facturar_al_cliente
        {
            get
            {
                return (facturar_tipo_receptor == 1 ? true : false);
            }
        }
    }
}
