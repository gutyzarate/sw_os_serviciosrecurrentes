﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW_OS_ServiciosRecurrentes.Models.ModelResult
{
    class OS_Empresas
    {
        public long id { get; set; }
        public string etiqueta { get; set; }
        public bool activo { get; set; }
    }
}
