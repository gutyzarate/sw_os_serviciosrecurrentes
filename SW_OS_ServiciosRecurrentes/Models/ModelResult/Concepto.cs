﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW_OS_ServiciosRecurrentes.Models.ModelResult
{
    public class Concepto
    {
        public long id { get; set; }
        public string servicio { get; set; }
        public string descripcion { get; set; }
        public decimal costo { get; set; }
        public string unidad { get; set; }
        public string concepto_gp { get; set; }
        public byte estatus { get; set; }
        public string equipo_de_trabajo { get; set; }
        public long sitio_id { get; set; }
        public String sitio_etiqueta { get; set; }
        public string concepto_gp_nombre { get; set; }
    }
}
