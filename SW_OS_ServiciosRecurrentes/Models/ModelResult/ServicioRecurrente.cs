﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW_OS_ServiciosRecurrentes.Models.ModelResult
{
    public class ServicioRecurrente
    {
        public long id { get; set; }
        public Int64 concepto_id { get; set; }
        public String servicio_recurrente_concepto_etiqueta { get; set; }
        public Int64 sitio_id { get; set; }
        public String servicio_recurrente_sitio_etiqueta { get; set; }
        public Int32 frecuencia { get; set; }
        public Int32 servicio_recurrente_frecuencia { get; set; }
        public Int64 tipo_operacion_id { get; set; }
        public String servicio_recurrente_tipo_operacion_etiqueta { get; set; }
        public String cliente_id { get; set; }
        public String servicio_recurrente_cliente_nombre { get; set; }
        public DateTime creado_el { get; set; }
        public DateTime? actualizado_el { get; set; }
        public DateTime? eliminado_el { get; set; }
        public DateTime? fecha_inicial { get; set; }
        public DateTime? fecha_final { get; set; }
        public DateTime? fecha_proximo_evento { get; set; }
        public String aduana_id { get; set; }
        public String seccion_aduana { get; set; }
        public String tipo_frecuencia { get; set; }
        public bool? acuerdo_de_cliente { get; set; }
        public long? usuario_alta { get; set; }
        public long? usuario_modificacion { get; set; }
    }
}
