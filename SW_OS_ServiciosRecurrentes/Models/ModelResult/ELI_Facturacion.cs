﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW_OS_ServiciosRecurrentes.Models.ModelResult
{
    class ELI_Facturacion
    {
        public string IMPOEXP { get; set; }
        public string PEDNMBR { get; set; }
        public string REGCEID { get; set; }
        public string ADUASECC { get; set; }
        public string OPREFEREN { get; set; }
        public string CLPEID { get; set; }
        public Int16 cancelada { get; set; }
    }
}
