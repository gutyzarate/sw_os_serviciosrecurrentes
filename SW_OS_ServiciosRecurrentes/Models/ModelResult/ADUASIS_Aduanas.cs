﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW_OS_ServiciosRecurrentes.Models.ModelResult
{
    class ADUASIS_Aduanas
    {
        public string id_aduana { get; set; }
        public string seccion_aduana { get; set; }
        public string razon_social { get; set; }
        public string razon_social_abreviada { get; set; }
        public string descripcion_oficial { get; set; }
    }
}
