﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW_OS_ServiciosRecurrentes.Models.ModelResult
{
    [Table("OS_tblOrdenesServicioConceptos")]
    public class OS_OrdenServicioConcepto
    {
        public long id { get; set; }
        public double? cargo_1_valor { get; set; }
        public double? cargo_2_valor { get; set; }
        public long? cargo_1_id { get; set; }
        public long? cargo_2_id { get; set; }
        public long concepto_id { get; set; }
        public long orden_servicio_id { get; set; }
        public bool realizado { get; set; }
        public bool? acuerdo { get; set; }
        public bool facturar_al_cliente { get; set; }
        public double? cantidad { get; set; }
        public string precuenta_cliente { get; set; }
        public string factura_folio_cliente { get; set; }
        public string precuenta_interna { get; set; }
        public string factura_folio_interna { get; set; }
        public long? usuario { get; set; }
        public DateTime? facturado_el { get; set; }
        public long? usuario_alta { get; set; }
        public DateTime? creado_el { get; set; }
        public long? usuario_modificacion { get; set; }
        public DateTime? actualizado_el { get; set; }
    }
}
