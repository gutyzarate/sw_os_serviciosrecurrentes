﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW_OS_ServiciosRecurrentes.Models.ModelResult
{
    class OS_TiposOperacion
    {
        public long id { get; set; }
        public String etiqueta { get; set; }
        public Int64 modelo_id { get; set; }
    }
}
