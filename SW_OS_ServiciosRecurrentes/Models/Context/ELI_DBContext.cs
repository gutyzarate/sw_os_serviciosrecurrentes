﻿using System.Data.Entity;

namespace SW_OS_ServiciosRecurrentes.Models.Context
{
    public class ELI_DBContext : DbContext
    {
        public ELI_DBContext() : base("name=ELI_DataConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer<ELI_DBContext>(null);
        }
    }
}

