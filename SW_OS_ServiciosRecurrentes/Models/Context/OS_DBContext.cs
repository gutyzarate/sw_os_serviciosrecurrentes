﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW_OS_ServiciosRecurrentes.Models.Context
{
    class OS_DBContext : DbContext
    {
        public OS_DBContext() : base("name=OS_DataConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer<OS_DBContext>(null);
        }

        public DbSet<ModelResult.OS_OrdenServicio> OrdenServicio { get; set; }
        public DbSet<ModelResult.OS_OrdenServicioConcepto> OrdenServicioConcepto { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder
              .Entity<ModelResult.OS_OrdenServicio>()
              .MapToStoredProcedures();

            modelBuilder
              .Entity<ModelResult.OS_OrdenServicioConcepto>()
              .MapToStoredProcedures();

            modelBuilder
                .Entity<ModelResult.OS_OrdenServicio>()
                .HasMany(t => t.conceptos)
                .WithOptional()
                .HasForeignKey(t => t.orden_servicio_id);
        }
    }
}
